#!/usr/bin/python3

import psycopg2
from pyonf import pyonf

config = """
db_host: /var/run/postgresql/
db_port: 5432
db_admin: postgres
db_admin_password: changeme
kwollect_db_name: kwdb
kwollect_user: kwuser
oardb_host: oar
oardb_port: 5432
oardb_name: oar2
oardb_user: oarreader
oardb_password: read
max_user_metrics_per_min: 1000
"""
config = pyonf(config)
globals().update(config)


def sql(cmd):
    global sql_cur
    # print(cmd)
    sql_cur.execute(cmd)


sql_conn = psycopg2.connect(
    dbname=kwollect_db_name,
    host=db_host,
    port=db_port,
    user=db_admin,
    password=db_admin_password,
)
sql_conn.autocommit = True
sql_cur = sql_conn.cursor()

print("Creating OAR integration in database...")
sql(
    f"""
SET LOCAL SESSION AUTHORIZATION default;
CREATE EXTENSION IF NOT EXISTS  postgres_fdw;
CREATE SERVER IF NOT EXISTS oar_server
  FOREIGN DATA WRAPPER postgres_fdw
  OPTIONS (host '{oardb_host}', port '{oardb_port}', dbname '{oardb_name}', use_remote_estimate 'true');
CREATE USER MAPPING IF NOT EXISTS FOR {kwollect_user} SERVER oar_server OPTIONS (user '{oardb_user}', password '{oardb_password}');
GRANT ALL PRIVILEGES ON FOREIGN SERVER oar_server TO {kwollect_user};
DROP SCHEMA IF EXISTS oar CASCADE;
CREATE SCHEMA IF NOT EXISTS oar;
GRANT USAGE ON SCHEMA oar TO {kwollect_user};
GRANT ALL PRIVILEGES ON SCHEMA oar TO {kwollect_user};


SET LOCAL SESSION AUTHORIZATION {kwollect_user};
IMPORT FOREIGN SCHEMA public FROM SERVER oar_server INTO oar;

CREATE OR REPLACE VIEW nodetime_by_oarjob AS
  SELECT DISTINCT jobs.job_id,
    to_timestamp(jobs.start_time::double precision) AS start_time,
    CASE
      WHEN jobs.stop_time = 0 THEN NULL::timestamp with time zone
      ELSE to_timestamp(jobs.stop_time::double precision)
    END AS stop_time,
    split_part(resources.network_address::text, '.'::text, 1) AS node
  FROM oar.jobs,
    oar.assigned_resources,
    oar.resources
  WHERE jobs.assigned_moldable_job = assigned_resources.moldable_job_id
    AND assigned_resources.resource_id = resources.resource_id
    AND resources.type::text = 'default'::text;

CREATE OR REPLACE VIEW nodetime_by_job AS
  SELECT * FROM nodetime_by_oarjob;

DROP MATERIALIZED VIEW IF EXISTS oar_runningjobs;
CREATE MATERIALIZED VIEW oar_runningjobs AS
  SELECT DISTINCT jobs.job_id,
    to_timestamp(jobs.start_time::double precision) AS start_time,
    split_part(resources.network_address::text, '.'::text, 1) AS node,
    job_user AS user,
    NOW() AS last_refresh
  FROM oar.jobs,
    oar.assigned_resources,
    oar.resources
  WHERE jobs.stop_time = 0
    AND jobs.state = 'Running'
    AND jobs.assigned_moldable_job = assigned_resources.moldable_job_id
    AND assigned_resources.resource_id = resources.resource_id
    AND resources.type::text = 'default'::text;


CREATE OR REPLACE VIEW promoted_metrics_job AS
    SELECT DISTINCT
        jobs.job_id,
        split_part(resources.network_address::text, '.'::text, 1) AS node,
        CASE WHEN job_types.type LIKE 'monitor=%'
          THEN right(job_types.type::text, -8)
          ELSE '.*' END
        AS metric_id
      FROM oar.jobs, oar.job_types, oar.assigned_resources, oar.resources
      WHERE jobs.job_id = job_types.job_id
        AND jobs.assigned_moldable_job = assigned_resources.moldable_job_id
        AND assigned_resources.resource_id = resources.resource_id
        AND resources.type::text = 'default'::text
        AND (jobs.state::text = 'Running'::text OR jobs.state::text = 'Launching'::text)
        AND job_types.type LIKE 'monitor%';


CREATE OR REPLACE FUNCTION api.update_promoted_metrics()
RETURNS SETOF promoted_metrics AS $$
BEGIN
  DELETE FROM promoted_metrics;
  RETURN QUERY INSERT INTO promoted_metrics(device_id, metric_id)
    SELECT DISTINCT node, metric_id FROM promoted_metrics_job
    RETURNING *;
END;
$$ LANGUAGE 'plpgsql';


DROP FUNCTION IF EXISTS api.insert_user_metrics;
CREATE OR REPLACE FUNCTION api.insert_user_metrics(JSON)
RETURNS INT AS $$

import time
import json
import socket
from plpy import spiexceptions


def get_orig_hostname_from_request_header():

    req = "SELECT current_setting('request.header.x-forwarded-for')"
    host_ip = ""
    try:
        host_ip = plpy.execute(req)[0]
        host_ip = dict(host_ip).get("current_setting", "").split(",")[0]
        assert host_ip != ""
    except (spiexceptions.UndefinedObject, AssertionError) as ex:
        raise Exception(f"Missing x-forwarded-for header (content: '{{host_ip}}'): {{ex}}")

    hostname_full = socket.gethostbyaddr(host_ip)[0]
    hostname = hostname_full.split(".")[0]
    if "-kavlan-" in hostname:
        hostname = "-".join(hostname.split("-")[0:2])
    return hostname


def get_authenticated_user_from_request_header():
    req = "SELECT current_setting('request.header.x-api-user-cn')"
    try:
        res = plpy.execute(req)[0]
    except spiexceptions.UndefinedObject as ex:
        # No authentication header
        return None

    user = dict(res).get("current_setting", "")
    if user == "unknown":
        return None

    return user


def get_current_reservations(cache_timeout=5):

    r = plpy.execute("SELECT extract(epoch from NOW()-last_refresh) as last_refresh FROM oar_runningjobs LIMIT 1")
    if len(r) == 0 or r[0]["last_refresh"] > cache_timeout:
        plpy.execute("REFRESH MATERIALIZED VIEW oar_runningjobs")
    return plpy.execute("SELECT node,STRING_AGG(oar_runningjobs.user, ',') AS user FROM oar_runningjobs GROUP BY node")


def check_max_insert_count(user, insert_metrics_count, insert_time):

    MAX_REQ_TIME = 60
    MAX_REQ_COUNT = {max_user_metrics_per_min}

    if not SD.get("reqs"):
        SD["reqs"] = {{}}
    if not SD.get("reqs").get(user):
        SD["reqs"][user] = {{}}

    previous_reqs = SD["reqs"][user]

    # previous_reqs is a dict = {{time1: req_count, time2: req_count, ...}}
    # containing number of previous insertion mades at each timeX for user
    # and ensuring that no timeX is older than MAX_REQ_TIME seconds

    for t in list(previous_reqs.keys()):
        if t < insert_time - MAX_REQ_TIME:
            del previous_reqs[t]

    previous_reqs_count = sum(previous_reqs.values())

    if previous_reqs_count + insert_metrics_count > MAX_REQ_COUNT:
        raise Exception(f"More than {{MAX_REQ_COUNT}} allowed metrics inserted during last {{MAX_REQ_TIME}} seconds")

    if not previous_reqs.get(int(insert_time)):
        previous_reqs[int(insert_time)] = 0
    previous_reqs[int(insert_time)] += insert_metrics_count


metrics = json.loads(args[0])
if not isinstance(metrics, list):
    metrics = [metrics]

time_int = round(time.time(), 6)
time_sql = plpy.execute(f"SELECT to_timestamp({{time_int}})")[0]["to_timestamp"]

src_node = get_orig_hostname_from_request_header()
user = get_authenticated_user_from_request_header() or None  # "user1,user2" if several jobs on same node

existing_reservation = get_current_reservations()  # uses a cache (5sec)

allowed_nodes = set()

for r in existing_reservation:
    if user:
        # Authenticated mode: all reserved nodes are allowed
        if user in r["user"].split(","):
            allowed_nodes.add(r["node"])
    else:
        # Unauthenticated mode: find user from current jobs and request source node
        if src_node == r["node"]:
            user = r["user"]
            allowed_nodes.add(r["node"])
            break
if not user:
    raise Exception(f"Cannot insert metrics from host '{{src_node}}' which does not belong to a running job for unauthenticated request")

# raise error if more than "max_user_metrics_per_min" metrics inserted during last 60 seconds by this user
check_max_insert_count(user, len(metrics), time_int)

for metric in metrics:
    if not metric.get("device_id"):
        metric["device_id"] = src_node
    if metric["device_id"] not in allowed_nodes:
        raise Exception(f"Cannot insert metrics for device {{metric['device_id']}} which does not belong to '{{user}}' running jobs")

    if "metric_id" not in metric or "value" not in metric:
        raise Exception("Missing mandatory 'metric_id' or 'value' in entry")

    if not metric.get("timestamp"):
        metric["timestamp"] = time_sql
    else:
        timestamp_diff = plpy.execute(f"SELECT extract(epoch FROM '{{time_sql}}'::timestamp - '{{metric['timestamp']}}'::timestamp) AS timestamp_diff")[0]["timestamp_diff"]
        if timestamp_diff < -5 or timestamp_diff > 3600:
            raise Exception("Cannot insert metrics with timestamp in the future or older than 1 hour")

    if not metric.get("labels"):
        metric["labels"] = {{}}
    metric["labels"]["__insert_time"] = time_int
    metric["labels"]["_insert_user"] = user

req = plpy.prepare(
    "INSERT INTO metrics(timestamp, device_id, metric_id, value, labels) "
    "SELECT timestamp, device_id, metric_id, value, labels "
    "FROM json_to_recordset($1) "
    "AS metrics(timestamp TIMESTAMPTZ, device_id TEXT, "
                "metric_id TEXT, value DOUBLE PRECISION, labels JSONB)",
    ["json"])
r = plpy.execute(req, [json.dumps(metrics)])
#plpy.warning(r, dir(r))
return int(r.nrows())
$$ LANGUAGE 'plpython3u' SECURITY DEFINER;
"""
)
print("Database setup for OAR integration done.")


def main():
    pass


if __name__ == "__main__":
    main()
